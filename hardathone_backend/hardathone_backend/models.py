from django.db import models
from django.forms import ModelForm, IntegerField
from datetime import datetime
from django.conf import settings
task_statuses = (
    "await_host_request",
    "host_executing",
    "await_client_check",
    "finished"
)
task_statuses_choices = ((x,x) for x in task_statuses)


def now():
    return datetime.now().replace(microsecond=0)


class TaskStatus:
    task_statuses = task_statuses
    status_types = dict((task_statuses[x], x+1) for x in range(len(task_statuses)))

    @classmethod
    def update(cls, status):
        current = cls.status_types.get(status)
        if not current:
            raise ValueError("Wrong task status")
        if current == len(cls.task_statuses):
            return cls.task_statuses[-1]
        new_status = dict((v,k) for k,v in cls.status_types.items())[current+1]
        return new_status

    @classmethod
    def downgrade(cls, status):
        current = cls.status_types.get(status)
        if not current:
            raise ValueError("Wrong task status")
        if current == 1:
            return cls.task_statuses[0]
        new_status = dict((v,k) for k,v in cls.status_types.items())[current-1]
        return new_status


class UserPhotos(models.Model):
    """User photos address"""
    user_id = models.IntegerField()
    image = models.ImageField(upload_to="photos/")

    @property
    def full_url(self):
        return settings.SITE_URL + self.image.url

    @property
    def url(self):
        return self.image.url

    @classmethod
    def client_get_photo_urls(cls, user_id):
        try:
            photos = cls.objects.filter(user_id=user_id)
            return [x.full_url for x in photos]
        except Exception as e:
            print(e)
            return None

    @classmethod
    def have_copy(cls, name):
        name = name.split('.')[0]
        objs = cls.objects.filter(image__contains=name)
        if objs:
            return objs[0]
        else:
            return None

class UserPhotosForm(ModelForm):
    class Meta:
        model = UserPhotos
        fields = ['image', 'host_id']

    host_id = IntegerField()


class HostUserRealtions():
    """"in production it can be model. For us there is only 1 host_id and user_id"""
    @classmethod
    def get_host_for_user(cls, user_id):
        if int(user_id) != 1:
            raise RuntimeError("user_id:{} Wat?".format(str(user_id)))
        return 1

    @classmethod
    def get_user_for_host(cls, host_id):
        if int(host_id) != 1:
            raise RuntimeError("host_id:{} Wat?".format(str(host_id)))
        return 1


class Task(models.Model):
    """
    Task from end_user for host.
    There are tons of DBMS bad requests.
    But we use sqlite, so who cares?
    """
    GET_PHOTOS = 'get_photos'
    TAKE_PHOTO = 'take_photo'

    GET_LEVEL_WATER = 'get_level_water'
    ADD_LEVEL_WATER = 'add_level_water'
    GET_GLOBAL_LEVEL_WATER = 'get_global_level_water'

    GET_LEVEL_FOOD = 'get_level_food'
    ADD_LEVEL_FOOD = 'add_level_food'
    GET_GLOBAL_LEVEL_FOOD = 'get_global_level_food'

    GET_REPORT = 'get_report'
    TASK_TYPES = (
        GET_PHOTOS,
        TAKE_PHOTO,
        GET_LEVEL_WATER,
        ADD_LEVEL_WATER,
        GET_GLOBAL_LEVEL_WATER,
        GET_LEVEL_FOOD,
        ADD_LEVEL_FOOD,
        GET_GLOBAL_LEVEL_FOOD,
        GET_REPORT
    )
    TASK_STATUSES = task_statuses

    TASK_CHOICES = (
        (GET_PHOTOS, GET_PHOTOS),
        (TAKE_PHOTO,TAKE_PHOTO),
        (GET_LEVEL_WATER,GET_LEVEL_WATER),
        (ADD_LEVEL_WATER,ADD_LEVEL_WATER),
        (GET_REPORT,GET_REPORT)
    )

    user_id = models.IntegerField()
    host_id = models.IntegerField()
    task_type = models.CharField(
        choices=TASK_CHOICES,
        max_length=50
    )
    status = models.CharField(
        choices=task_statuses_choices,
        default=task_statuses[0],
        max_length=50
    )
    result = models.TextField(max_length=100, default="")
    datetime = models.DateTimeField(default=now)

    def update(self, result=None):
        if result:
            self.result = result
        self.status = TaskStatus.update(self.status)
        self.save()

    def downgrade(self):
        self.status = TaskStatus.downgrade(self.status)
        self.save()

    def tasks_for_host(self, host_id):
        return self.objects.filter(host_id=host_id).filter(status=task_statuses[0])

    def results_for_user(self, user_id):
        return self.objects.filter(user_id=user_id).filter(status=task_statuses[2])

    @classmethod
    def get_task(cls, task_id, user_id=None, host_id=None):
        pretender = cls.objects.filter(id=task_id)
        if user_id:
            pretender = pretender.filter(user_id=user_id)
        if host_id:
            pretender = pretender.filter(host_id=host_id)
        if not pretender:
            return False
        if len(pretender) > 1:
            return False
        return pretender[0]

    @classmethod
    def client_get_task_results(cls, task_id, user_id):
        task = cls.get_task(task_id, user_id=user_id)
        if not task:
            return None, None
        if task.status == task_statuses[2]:
            task.status = TaskStatus.update(task.status)
            task.save()
        return task.status, task.result

    @classmethod
    def client_post_task(cls, user_id, host_id, task_type):
        try:
            task = cls.objects.create(user_id=user_id, host_id=host_id, task_type=task_type)
            return True, task
        except Exception as e:
            print(str(e))
            return False, None

    @classmethod
    def get_tasks_with_status(cls, task_status):
        if not task_status in cls.TASK_STATUSES:
            raise ValueError("Wrong task status")
        return cls.objects.filter(status=task_status)

    #@classmethod
    #def client_get_photo_urls(cls, user_id):
    #    photos = cls.objects.filter(user_id=user_id).filter(task_type=cls.TAKE_PHOTO).filter(status=task_statuses[-1])
    #    return [x.result for x in photos]

    @classmethod
    def host_get_oldest_task(cls, host_id):
        awaiting_tasks = cls.objects.filter(host_id=host_id).filter(status=task_statuses[0]).order_by('datetime')
        try:
            task = awaiting_tasks[0]
            return task

        except IndexError:
            return None

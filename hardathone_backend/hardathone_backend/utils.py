from rest_framework.permissions import BasePermission


class GotValuePermission(BasePermission):
    value = None
    checker = lambda self, x: x

    def has_permission(self, request, view):
        if not self.value:
            raise NotImplementedError()

        if request.method == 'GET':
            value = request.query_params.get(self.value)
        else:
            value = request.data.get(self.value)

        try:
            value = self.checker(value)
        except ValueError:
            return False
        return bool(value)


class UserIdPermission(GotValuePermission):
    value = "user_id"
    checker = lambda self, x: int(x)


class HostIdPermission(GotValuePermission):
    value = "host_id"
    checker = lambda self, x: int(x)
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .utils import UserIdPermission, HostIdPermission
from .models import Task, UserPhotos, HostUserRealtions, UserPhotosForm


class ClientTaskView(APIView):
    """
    Interface for mobile client.
    GET: results for finished previously ordered tasks
    POST: set new tasks
    """
    permission_classes = UserIdPermission,

    def get(self, request):
        user_id = request.query_params.get("user_id")
        task_id = request.query_params.get("task_id")
        if not task_id:
            return Response(
                {"error": "Task_id not given"},
                status=status.HTTP_400_BAD_REQUEST
            )
        cur_task_status, cur_task_result = Task.client_get_task_results(task_id, user_id)
        if not cur_task_status:
            return Response(
                {"error": "Task_id and user_id combination not found"},
                status=status.HTTP_400_BAD_REQUEST
            )
        return Response(
            {
                "task_id": task_id,
                "task_status": cur_task_status,
                "result": cur_task_result
            }
        )

    def post(self, request):
        user_id = request.data.get("user_id")
        task_type = request.data.get("task_type")
        if not task_type:
            return Response(
                {"error": "Task_type not given"},
                status=status.HTTP_400_BAD_REQUEST
            )
        if not task_type in Task.TASK_TYPES:
            return Response(
                {"error": "Task_type not recognized"},
                status=status.HTTP_400_BAD_REQUEST
            )
        host_id = HostUserRealtions.get_host_for_user(user_id)
        succeeded, task = Task.client_post_task(user_id=user_id, host_id=host_id, task_type=task_type)
        if not succeeded:
            return Response(
                {"error": "Failed to create task"},
                status=status.HTTP_400_BAD_REQUEST
            )
        return Response({"task_id": task.id})


class PhotoView(APIView):
    def get(self, request):
        user_id = int(request.query_params.get("user_id"))
        if not user_id:
            return Response({"error":"no user_id"}, status=status.HTTP_400_BAD_REQUEST)
        photo_urls = UserPhotos.client_get_photo_urls(user_id)
        return Response({"photo_urls": photo_urls})

    def post(self, request):
        host_id = int(request.data.get("host_id"))
        if not host_id:
            return Response({"error": "no host_id"}, status=status.HTTP_400_BAD_REQUEST)
        name = request.FILES.get('image').name
        copy = UserPhotos.have_copy(name)
        if copy:
            return Response({"url": copy.full_url})
        form = UserPhotosForm(request.POST, request.FILES)
        if form.is_valid():
            photo = form.save(commit=False)
            photo.user_id = HostUserRealtions.get_user_for_host(host_id)
            photo.save()
            url = None
            if photo:
                url = photo.full_url
            return Response({"url": url})
        else:
            return Response({"error":"Failed to save"}, status=status.HTTP_400_BAD_REQUEST)


class HostTaskView(APIView):
    """
    **Use Case**
        Get oldest task ordered by client.
        Update task status when executet
    **Example Requests**
          GET /api/extended/libraries/
          UPDATE
    **Response Values**
        * count: The number of courses in the edX platform.
        * next: The URI to the next page of courses.
        * previous: The URI to the previous page of courses.
        * num_pages: The number of pages listing courses.
        * results:  A list of courses returned. Each collection in the list
          contains these fields.
            * id: The unique identifier for the course.
              "course".
            * org: The organization specified for the course.
            * course: The course number.
    """
    permission_classes = HostIdPermission,

    def get(self, request):
        host_id = request.query_params.get("host_id")
        task_id = request.query_params.get("task_id")
        try:
            if task_id:
                task_id = int(task_id)
        except TypeError:
            return Response({"error":"bad task_id"},status=status.HTTP_400_BAD_REQUEST)
        if task_id:
            task = Task.get_task(task_id=task_id)
            if not task:
                return Response({"error":"no task with such id"}, status=status.HTTP_400_BAD_REQUEST)
            return Response({
                "task_id":task.id,
                "task_type":task.task_type,
                "task_status":task.status,
                "result": task.result
            })

        tasks_status = request.query_params.get("task_status")
        if tasks_status:
            if tasks_status not in Task.TASK_STATUSES:
                return Response({"error": "wrong status"}, status=status.HTTP_400_BAD_REQUEST)

            statused_tasks = Task.get_tasks_with_status(tasks_status)
            ids = [t.id for t in statused_tasks]
            return Response({"task_ids":ids})

        task = Task.host_get_oldest_task(host_id)
        if not task:
            return Response({"task_id": None, "task_type": None})
        task.update()
        return Response({"task_id": task.id, "task_type":task.task_type})

    def post(self, request):
        task_id = request.data.get("task_id")
        if not task_id:
            return Response({"error":"task_id not specified"}, status=status.HTTP_400_BAD_REQUEST)
        task_result = request.data.get("result")
        if not task_result:
            return Response({"error": "result not specified"}, status=status.HTTP_400_BAD_REQUEST)
        task = Task.get_task(task_id)
        if task.status != Task.TASK_STATUSES[1]:
            messages = {
                Task.TASK_STATUSES[0]:"this task was not taken by host yet",
                Task.TASK_STATUSES[2]:"this task was already executed earlier",
                Task.TASK_STATUSES[3]: "this task was already finished",
            }
            return Response({"error": messages[task.status]}, status=status.HTTP_400_BAD_REQUEST)
        task.update(result=str(task_result))
        return Response()
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from .views import ClientTaskView, PhotoView, HostTaskView

urlpatterns = [
    #url(r'^admin/', include(admin.site.urls)),
    url(r'^api/client$', ClientTaskView.as_view()),
    url(r'^api/photos$', PhotoView.as_view()),
    url(r'^api/host$', HostTaskView.as_view())
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hardathone_backend', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserPhotos',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('user_id', models.IntegerField()),
                ('image', models.ImageField(upload_to='photos/')),
            ],
        ),
    ]

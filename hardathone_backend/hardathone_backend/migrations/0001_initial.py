# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import hardathone_backend.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('user_id', models.IntegerField()),
                ('host_id', models.IntegerField()),
                ('task_type', models.CharField(choices=[('get_photos', 'get_photos'), ('take_photo', 'take_photo'), ('get_level_water', 'get_level_water'), ('add_level_water', 'add_level_water'), ('get_report', 'get_report')], max_length=50)),
                ('status', models.CharField(choices=[('await_host_request', 'await_host_request'), ('host_executing', 'host_executing'), ('await_client_check', 'await_client_check'), ('finished', 'finished')], default='await_host_request', max_length=50)),
                ('result', models.TextField(default='', max_length=100)),
                ('datetime', models.DateTimeField(default=hardathone_backend.models.now)),
            ],
        ),
    ]

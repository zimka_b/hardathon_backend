from rest_framework import serializers
from .models import UserPhotos


class UserPhotosSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPhotos
        fields = ('id', 'user_id', 'image')